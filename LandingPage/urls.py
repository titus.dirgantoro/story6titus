from django.urls import path
from . import views

app_name = 'LandingPage'

urlpatterns = [
    path('', views.index, name='index'),
    path('TambahStatus/', views.TambahStatus, name='tambahstatus'),
]