from django.shortcuts import render,redirect
from .models import Status
from datetime import datetime
from .forms import FormStatus
from django.views.decorators.http import require_POST
import pytz

# Create your views here.
def index(request):
	list_status = Status.objects.order_by('-date')
	form = FormStatus()
	context = {'list_status' : list_status , 'form' : form}
	return render(request, 'landingpage.html', context)

@require_POST
def TambahStatus(request):
	form = FormStatus(request.POST)

	if form.is_valid():
		status_baru = Status(statusnow = request.POST['statusnow'])
		status_baru.save()
	return redirect('LandingPage:index')



